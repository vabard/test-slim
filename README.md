Тестовое задание для PHP-программиста
=====================================

Первый запуск проекта:
--------------

### В корне проекта:
#### Скопировать .env в .env.local
cp .env .env.local

#### Исправить в .env.local значение переменную DATABASE так, чтобы логин, пароль и название БД соответствовали переменным MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE из файла docker-compose.yaml, а имя хоста соответстовало имени службы БД из docker-compose (database)

#### Собрать и запустить контейнеры
docker-compose -up -d --build

#### Зайти в контейнер app
docker-compose exec app /bin/sh

#### Запустить миграции 
php bin/console orm:schema-tool:update --force

#### Запустить заполнение Movies тестовыми данными
php bin/console fetch:trailers

#### Запустить встроенный в PHP веб-сервер
cd public
php -S 0.0.0.0:8080

#### Приложение должно открываться браузером по адресу http://localhost:8080