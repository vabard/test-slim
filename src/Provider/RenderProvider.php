<?php declare(strict_types=1);

namespace App\Provider;

use App\Container\Container;
use App\Support\Config;
use App\Support\ServiceProviderInterface;
use Psr\Container\ContainerInterface;
use Slim\Routing\RouteCollector;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

class RenderProvider implements ServiceProviderInterface
{
    public function register(Container $container): void
    {
        $container->set(Environment::class, static function (ContainerInterface $container) {
            $config = $container->get(Config::class);
            $loader = new FilesystemLoader($config->get('templates')['dir']);
            $cache = $config->get('templates')['cache'];

            $options = [
                'cache' => empty($cache) || $container->get(Config::class)->get('environment') === 'dev' ? false : $cache,
            ];

            $twig = new Environment($loader, $options);

            $twig->addFunction(new TwigFunction('url_for', function (
                string $routeName,
                array $data = [],
                array $queryParams = []) use ($container) {
                    $routeCollector = $container->get(RouteCollector::class);

                    return $routeCollector->getRouteParser()->urlFor($routeName, $data, $queryParams);
                })
            );

            return $twig;
        });
    }
}
